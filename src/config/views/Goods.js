export default {
  'shop_guid': '3F596B1ED962B52FFC7B412DCBCE69AF',
  'title': 'P30 2018款 折叠桨（正桨）',
  'sub_title': '1个',
  'sku': [],
  'available_stock': 13,
  'stock_percent': 10,
  'weight': 0,
  'detail': '<p><br/></p><p style=\'white-space: normal;\'><span style=\'color: rgb(255, 0, 0);\'><strong><span style=\'font-size: 20px;\'>适用机型：</span></strong></span></p><p style=\'white-space: normal;\'><span style=\'color: rgb(255, 0, 0);\'><strong><span style=\'font-size: 20px;\'>P30 2018款 植保无人机</span></strong></span></p><p style=\'white-space: normal;\'><br/></p><p><br/></p>',
  'logistics_cost': '0',
  'description_pics': [],
  'shop_name': '极飞商城',
  'shop_company_name': '广州极飞科技有限公司',
  'shop_description': '主营极飞产品零部件~',
  'shop_logo': 'http://agri-private.static.xag.cn/v3/upload/2018/04/04/dbaa24c8a03b4b920c0550b980e33765.jpeg?imageView2/0/w/100/h/100&e=1541673764&token=wYqu938c9qi_H_uUgnE_B36feoECpNZuZ1cZQw1S:szTJJ0hNWYkF1lUQpFUcQCRMSPs=',
  'pics': [
    'http://agri-private.static.xag.cn/v3/upload/2018/05/22/6b5f8910c89af4ace4aeb87d7be79a70.png?imageView2/0/w/750/h/750&e=1541673764&token=wYqu938c9qi_H_uUgnE_B36feoECpNZuZ1cZQw1S:iTHJXoaZiiXT6fOGM9SdUQEz5HY=',
    'http://agri-private.static.xag.cn/v3/upload/2018/05/22/c7fe0ac4b66b0dace4f34846e20b65fc.png?imageView2/0/w/750/h/750&e=1541673764&token=wYqu938c9qi_H_uUgnE_B36feoECpNZuZ1cZQw1S:vkd4_tm7x2Gxbit8mcbyRYyqbRw='
  ],
  'product_guid': '9E4D4B06598FD7976C4F829FD3B99C0A',
  'buy': 1,
  'buy_notice': '',
  'product_parent_guid': '',
  'price': 420
}